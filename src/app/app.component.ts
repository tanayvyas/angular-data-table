import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {AppServices} from "./app.service";
import {Sample} from "./Sample";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AppServices]
})
export class AppComponent implements AfterViewInit {
/** displayed columns values */
  displayedColumns =['id','name','phone','email','company','date_entry','org_num','address_1','city','zip',
  'geo','pan','pin','status','fee','guid','date_exit','date_first','date_recent','url'];

  /** setting the datasource value  */
  dataSource = new MatTableDataSource<Sample>();

    /** Constructor */
  constructor(private apiService: AppServices){
    
      }
  /** Matpaginator object */
  @ViewChild(MatPaginator) paginator: MatPaginator;
  
  /**
   * Set the paginator after the view init since this component will
   * be able to query its view for the initialized paginator.
   */
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
   /**posts the data to the API  */
  rowClicked(row: any): void {
      this.apiService.postSampleData(row);
  }

 /** gets the data from the sample json file  */
  ngOnInit() {
    this.apiService.getSampleData().subscribe(
      data => {
        this.dataSource.data = data;
      }
    );
   }
}


