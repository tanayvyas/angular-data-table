import { Component, Input } from '@angular/core';
import { Injectable }     from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AppServices{

    constructor(private http: Http) {
         var obj;
         this.getSampleData().subscribe(data => obj=data, error => console.log(error));
    }
/** Gets the data from the sample json and return the observable */
    public getSampleData(): Observable<any> {
         return this.http.get("assets/sample_data.json")
                         .map((res:any) => res.json())
                         .catch((error:any) => error);
     }
/** Post request for processing ROW id clicked. */
     public postSampleData(row):void {
        console.log(row);
    }
}